describe('Search form tests', ()=>{
    beforeEach(function(){
        cy.visit('/');
        cy.login();
        cy.get('a[href="/archives"]').click();
    });

    it('displays no results info', ()=>{
        cy.get('input.css-1qq8djj.css-673enp4').type('notfound123');
        cy.get('div.css-1xn3edc.css-7fiqjt3').find('img').should('be.visible');
        cy.get('h2').contains('Darn, no results found').should('be.visible');
    })

    it('clears query by "x" button', ()=>{
        cy.get('input.css-1qq8djj.css-673enp4').type('clearme');
        cy.clear();
        cy.get('input.css-1qq8djj.css-673enp4').should('have.value', '');
        cy.get('ul.css-1a35ai7.css-9r5fuh0').find('li').should('have.length', 5);
        cy.get('div.css-k008qs.css-1o5oqer4').should('contain', '5 chats');
    })

    it('clears query by "clear your search" link', ()=>{
        cy.get('input.css-1qq8djj.css-673enp4').type('clearme');
        cy.get('ul.css-1a35ai7.css-9r5fuh0').find('li').should('have.length', 0);
        cy.contains('span', 'clear your search').click();
        cy.get('ul.css-1a35ai7.css-9r5fuh0').find('li').should('have.length', 5);
    })

    it('finds results for query', ()=>{
        cy.get('input.css-1qq8djj.css-673enp4').type('integration with Facebook');
        cy.get('ul.css-1a35ai7.css-9r5fuh0').find('li').should('contain', "Client no2");
        cy.get('div.css-k008qs.css-1o5oqer4').should('contain', '1 chat');
    })
    //Test below should fail
    it('finds part of word', ()=>{
        cy.get('input.css-1qq8djj.css-673enp4').type('Face');
        cy.get('ul.css-1a35ai7.css-9r5fuh0').find('li').should('have.length', 1);
        cy.get('ul.css-1a35ai7.css-9r5fuh0').find('li').should('contain', "Client no2");
    })

    it('checks if leading and trailing spaces are trimmed properly', ()=>{
        cy.get('input.css-1qq8djj.css-673enp4').type(' Facebook');
        cy.get('ul.css-1a35ai7.css-9r5fuh0').find('li').should('have.length', 1).and('contain', "Client no2");
        cy.clear();
        cy.get('input.css-1qq8djj.css-673enp4').type('Facebook ');
        cy.get('ul.css-1a35ai7.css-9r5fuh0').find('li').should('have.length', 1).and('contain', "Client no2");
        cy.get('ul.css-1a35ai7.css-9r5fuh0').find('li').should('contain', "Client no2");
    })

    it('checks case insensitive', ()=>{
        cy.get('input.css-1qq8djj.css-673enp4').type('FaCeBOOK');
        cy.get('ul.css-1a35ai7.css-9r5fuh0').find('li').should('have.length', 1).and('contain', "Client no2");
    })
})