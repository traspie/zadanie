// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
// Logging in to planner
Cypress.Commands.add("login", (userRole) => {
    const types = {
        // Change default user below this line
        defaultUser: {
            email: "m.debski+frontend_tests@livechatinc.com",
            password: "test1@3$"
        }
    };
        let user= types[userRole];

    if (!userRole){
        user= types.defaultUser;
    }
    cy.get('input[type=email]')
        .type(user['email'])
        .should('have.value', user['email']);

    cy.get('input[type=password]')
        .type(user['password'])
        .should('have.value', user['password']);
    cy.get('button.button.red').click();
});

// Clearing search form query
Cypress.Commands.add('clear', ()=>{
    cy.get('button[title="Clear"]').click();
})